<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\BillsRecord;
use App\Models\Admin\CashBook;
use App\Models\Admin\Company;
use App\Models\Admin\CountryPara;
use App\Models\Admin\CustomerDetail;
use App\Models\Admin\CustomerPara;
use App\Models\Admin\CustomerPriceDetail;
use App\Models\Admin\EmployeeParameter;
use App\Models\Admin\Member;
use App\Models\Admin\MerchandisePara;
use App\Models\Admin\Notifications;
use App\Models\Admin\Package;
use App\Models\Admin\ZoneMaster;
use App\Permission;
use App\RolePermission;
use App\UserPermission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\User;
use App\Role;
use Hash;
use App\Models\Admin\LocationHierarachy;
use App\Models\Admin\BranchPara;
use Input;
use Excel;
use App;
use Session;
use Illuminate\Support\Collection;
use App\Models\Admin\BillStockPara;
use  App\Repositories\Backend\BranchPara\BranchParaInterface;
use App\Models\Admin\BankAccount;
use App\Models\Admin\Item;

class UserController extends DashboardController
{
    public function __construct(BranchParaInterface $branch_para)
    {
        parent::__construct();
        $this->branch_para = $branch_para;
    }

    public function customerIndex()
    {

        Session::forget('function_type_b');
        Session::forget('function_type_a');
        $this->admin_data['customer_paras'] = CustomerPara::orderBy('id', 'desc')->where('branch_code', '=', $this->admin_data['login_user']->branch_code)->get();
        $this->admin_data['master_companies'] = Company::all();
        $this->admin_data['country_para'] = CountryPara::orderBy('country_name', 'desc')->get();
        $this->admin_data['branch_para'] = BranchPara::where('branch_code', '=', $this->admin_data['login_user']->branch_code)->first();
        return view('admin.user.customer.index', $this->admin_data);
    }

    public function package()
    {
        $this->admin_data['packages'] = Package::orderby('id', 'desc')->where('category','=',$this->admin_data['login_user']->category)->get();
        return view('admin.package.index', $this->admin_data);
    }

    public function packageStore(Request $request)
    {
        $data=$request->all();
        $data['category']=$this->admin_data['login_user']->category;
        Package::create($data);
        Session::flash('successMsg', 'Package saved successfully');
        return response()->json(['success' => 'true'], 200);
    }

    public function selectedPackage(Request $request)
    {
        if ($request->month == null)
            return response()->json(['success' => false], 200);
        if($request->month=='Due'){
            $bill_record=BillsRecord::where('membership_no','=',$request->membership_no)->where('due_amount','!=',0)->orderBy('id','desc')->first();
            return response()->json(['success' => true, 'message' => '', 'data' => ['price' => $bill_record->due_amount, 'user_valid_date' => $bill_record->valid_date]], 200);
        }
        $month = Package::where('month', '=', $request->month)->where('category','=',$this->admin_data['login_user']->category)->first();
        $time = strtotime($request->admission_date);
        if($request->valid_date_change_type=='User Old Date'){
            $date=BillsRecord::where('membership_no','=',$request->membership_no)->orderBy('id','desc')->first();
            $time=strtotime($date->valid_date);
        }
        $user_valid_date = date("Y-m-d", strtotime("+" . $request->month . " month", $time));
        return response()->json(['success' => true, 'message' => '', 'data' => ['price' => $month->price, 'user_valid_date' => $user_valid_date]], 200);
    }

    public function validDate(Request $request)
    {
        $time = strtotime($request->admission_date);
        $user_valid_date = date("Y-m-d", strtotime("+24 month", $time));
        return response()->json(['success' => true, 'message' => '', 'data' => ['user_valid_date' => $user_valid_date]], 200);

    }


    public function deletePackage($id)
    {
        $package = Package::find($id);
        $package->delete();
        Session::flash('successMsg', 'Package deleted successfully');
        return response()->json(['success' => true, 'message' => 'Package Deleted', 'data' => null], 200);
    }

    public function editPackage($id)
    {
        $package = Package::find($id);
        return response()->json(['success' => true, 'message' => 'Package Editted', 'data' => ['package' => $package]], 200);
    }

    public function updatePackage(Request $request)
    {
        Package::where('id', '=', $request->package_id)->update(['month' => $request->month_pop, 'price' => $request->price_pop]);
//        $package =Package::find($id);
        Session::flash('successMsg', 'Package updated successfully');
        return response()->json(['success' => true, 'message' => 'Package Deleted', 'data' => null], 200);

    }
    public function deleteItem($id)
    {
        $item = Item::find($id);
        $item->delete();
        Session::flash('successMsg', 'Package deleted successfully');
        return response()->json(['success' => true, 'message' => 'Package Deleted', 'data' => null], 200);
    }

    public function editItem($id)
    {
        $item = Item::find($id);
        return response()->json(['success' => true, 'message' => 'Package Editted', 'data' => ['item' => $item]], 200);
    }

    public function updateItem(Request $request)
    {
        Item::where('id', '=', $request->item_id)->update(['item_name' => $request->item_name, 'price' => $request->price]);
//        $package =Package::find($id);
        Session::flash('successMsg', 'Package updated successfully');
        return response()->json(['success' => true, 'message' => 'Package Deleted', 'data' => null], 200);

    }
    public function itemCategory()
    {
        $this->admin_data['items'] =Item::orderby('id', 'desc')->get();
        return view('admin.category.index', $this->admin_data);
    }

    public function itemCategoryStore(Request $request)
    {
        $data=$request->all();
//        $data['category']=$this->admin_data['login_user']->category;
        Item::create($data);
        Session::flash('successMsg', 'Package saved successfully');
        return response()->json(['success' => 'true'], 200);
    }


    public function billProfile($id){
        $this->admin_data['bill_record'] = BillsRecord::select('members.*', 'bills_record.*')
            ->leftJoin('members', 'members.membership_no', '=', 'bills_record.membership_no')
            ->where('bills_record.id','=',$id)
            ->orderBy('bills_record.id', 'desc')
            ->first();
        $now = time(); // or your date as well
        $your_date = strtotime($this->admin_data['bill_record']->valid_date);
        $datediff = $your_date- $now;

        $this->admin_data['date_diff']=(round($datediff / (60 * 60 * 24)));
      return view('admin.bill_record.bill_profile',$this->admin_data);
    }
    public function billRecord()
    {
//        $bill = BillStockPara::orderBy('id', 'desc')->first();

//        $length = strlen($bill->prefix);
//        $query = BillsRecord::select(DB::raw("MAX(CAST(SUBSTRING(bill_no," . $length . "+1) AS UNSIGNED)+1) AS bill_no"))->whereRaw("SUBSTRING(bill_no,1," . $length . ") ='" . $bill->prefix . "'")->first();
//        if ($query->bill_no != null) {
//            $this->admin_data['bill_no'] = $bill->prefix . $query->bill_no;
//        } else {
//            $this->admin_data['bill_no'] = $bill->prefix . '1';
//        }
        $length = 0;
        $query = BillsRecord::select(DB::raw("MAX(CAST(SUBSTRING(bill_no," . $length . "+1) AS UNSIGNED)+1) AS bill_no"))->first();
        if ($query->bill_no != null) {

            $this->admin_data['bill_no'] = $query->bill_no;
        } else {
            $this->admin_data['bill_no'] = 1;
        }
        $this->admin_data['packages']=Package::where('category','=',$this->admin_data['login_user']->category)->get();
        $this->admin_data['members'] = Member::all();
        if($this->admin_data['login_user']->category=='fitness'){
            $this->admin_data['items']=Item::all();
        }
        return view('admin.bill_record.index', $this->admin_data);
    }

    public function cancelBill(Request $request)
    {
        BillsRecord::where('bill_no', '=', $request->bill_no)->update(['status' => 'Cancelled']);
        Session::flash('successMsg', 'Bill NO succesfully cancelled');
        return response()->json(['success' => true], 200);
    }

    public function storeBillRecord(Request $request)
    {
        $bil_record = new BillsRecord();
        $bil_record->date = isset($request->date) ? $request->date : '0';
        $bil_record->membership_no = isset($request->membership_no) ? $request->membership_no : '';
        $bil_record->name = isset($request->member_name) ? $request->member_name : '';
//        $query = BillsRecord::select(DB::raw("MAX(bill_no)+1 AS bill_no"))->first();
//        if ($query->bill_no != null) {
        $bil_record->bill_no = $request->bill_no;
//        } else {
//            $bil_record->bill_no = 1;
//        }
        if($request->package =='Due'){
            $bill_record=BillsRecord::where('membership_no','=',$request->membership_no)->where('due_amount','!=',0)->orderBy('id','desc')->first();
            $bill_record->due_amount=0;
            $bill_record->save();
        }
        $bil_record->package = isset($request->package) ? $request->package : '0';
        $bil_record->amount = isset($request->amount) ? $request->amount : '0';
        $bil_record->discount = isset($request->discount) ? $request->discount : '0';
        $bil_record->paid_amount = isset($request->paid_amount) ? $request->paid_amount : '0';
        $bil_record->due_amount = isset($request->due_amount) ? $request->due_amount : '0';
        $bil_record->remarks = isset($request->remarks) ? $request->remarks : '';
        $bil_record->valid_date = isset($request->user_valid_date) ? $request->user_valid_date : null;
        $bil_record->category=$this->admin_data['login_user']->category;
        $bil_record->save();
        BillsRecord::where('membership_no','=',$request->membership_no)->where('bill_no','!=',$request->bill_no)->update(['status'=>'Completed']);

        $bil_record = new CashBook();
        $bil_record->date = isset($request->date) ? $request->date : '0';
        $bil_record->particular = isset($request->member_name) ? $request->member_name : '';
        $bil_record->debit_amount = isset($request->paid_amount) ? $request->paid_amount : '';
        $bil_record->bill_no=$request->bill_no;
        $bil_record->credit_amount = '0.00';
        $bil_record->category=$this->admin_data['login_user']->category;
        $bil_record->save();

        
//        $member = Member::where('membership_no', '=', $request->membership_no)->first();
//        if ($member) {
//            $member->user_valid_date = $request->user_valid_date;
//            $member->save();
//        }


        Session::flash('successMsg', $bil_record->bill_no . ' Record Added successfully');
        return response()->json(['success' => true, 'message' => 'New Bill Record Added successfully', 'data' => null], 200);

    }
  public function statusChange(Request $request){
        $user=Member::where('membership_no','=',$request->membership_no)->first();
        $user->user_status=$request->user_status;
        $user->save();
                Session::flash('successMsg', 'Status Changed successfully');


    }

    public function listBillRecord()
    {
        $this->admin_data['bill_records'] = BillsRecord::select('members.*', 'bills_record.*')
          ->where('bills_record.category','=',$this->admin_data['login_user']->category)
            ->leftJoin('members', 'members.membership_no', '=', 'bills_record.membership_no')
            ->orderBy('bills_record.id', 'desc')
            ->get();
        return view('admin.bill_record.list', $this->admin_data);
    }

    public function addUser()
    {
        $length = 0;
        $query = BillsRecord::select(DB::raw("MAX(CAST(SUBSTRING(bill_no," . $length . "+1) AS UNSIGNED)+1) AS bill_no"))->first();
        if ($query->bill_no != null) {

            $this->admin_data['bill_no'] = $query->bill_no;
        } else {
            $this->admin_data['bill_no'] = 1;
        }
        $bill = BillStockPara::orderBy('id', 'desc')->where('type', '=', 'membership')
            ->where('category','=',$this->admin_data['login_user']->category)->first();
        if($bill ==null){
            return 'No bill issued';
        }

        $length = strlen($bill->prefix);
        $query = Member::select(DB::raw("MAX(CAST(SUBSTRING(membership_no," . $length . "+1) AS UNSIGNED)+1) AS membership_no"))->whereRaw("SUBSTRING(membership_no,1," . $length . ") ='" . $bill->prefix . "'")
            ->where('category','=',$this->admin_data['login_user']->category)->first();
        if ($query->membership_no != null) {
            $this->admin_data['membership_no'] = $bill->prefix . $query->membership_no;
        } else {
            $this->admin_data['membership_no'] = $bill->prefix . '1';
        }
        $this->admin_data['packages'] = Package::all();
        return view('admin.user.add', $this->admin_data);
    }

    public function storeUser(Request $request)
    {
        $member_check = Member::where('membership_no', '=', $request->membership_no)->first();
        if ($member_check) {
            return response()->json(['success' => true, 'message' => 'Already saved', 'data' => null], 200);

        }
        $data = $request->all();
        DB::beginTransaction();
        try {
            $input = [
                'membership_no', 'name', 'address', 'user_valid_date', 'gender', 'admission_date', 'package_rate', 'email', 'contact', 'photo', 'user_status', 'is_member','blood_group'
            ];


            foreach ($input as $i) {

                $data[$i] = isset($data[$i]) ? $data[$i] : '';
            }
            $data['is_member'] = isset($request->is_member) ? $request->is_member : 0;

            $data['user_status'] = 'Active';
            $data['category'] = $this->admin_data['login_user']->category;
            Member::create($data);
            if ($this->admin_data['login_user']->category != 'gym'){
                $bil_record = new BillsRecord();
            $bil_record->name = isset($request->name) ? $request->name : '';
            $bil_record->date = isset($request->admission_date) ? $request->admission_date : '';
            $bil_record->membership_no = isset($data['membership_no']) ? $data['membership_no'] : '';
            $bil_record->bill_no = isset($request['bill_no']) ? $request['bill_no'] : '';
            $bil_record->package = '';


            $bil_record->amount = isset($request->paid_amount) ? $request->paid_amount : '0';
            $bil_record->discount = isset($request->discount) ? $request->discount : '0';
            $bil_record->paid_amount = isset($request->paid_amount) ? $request->paid_amount : '0';
            $bil_record->name = isset($request->name) ? $request->name : '0';
            $bil_record->name = isset($request->name) ? $request->name : '';
            $bil_record->due_amount = isset($request->due_amount) ? $request->due_amount : '0';
            $bil_record->remarks = 'Admission';
            $bil_record->category = $this->admin_data['login_user']->category;

            $bil_record->save();
        }

            $bil_record = new CashBook();
            $bil_record->date = isset($request->admission_date) ? $request->admission_date : '';
            $bil_record->particular = isset($request->name) ? $request->name : '';
            $bil_record->debit_amount = isset($request->paid_amount) ? $request->paid_amount : '';
            $bil_record->credit_amount = '0.00';
            $bil_record->category='fitness';
            $bil_record->save();


            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('successMsg', $e->getMessage());
            dd($e->getMessage());
            // something went wrong
        }
        Session::flash('successMsg', 'User added Successfully');

        return response()->json(['success' => true, 'message' => 'Successfully saved', 'data' => null], 200);
    }

    public function deleteBillRecord(Request $request)
    {
        $bill_record = BillsRecord::find($request->record_id);
        $bill_record->delete();
        $cash_book=CashBook::where('bill_no','=',$request->record_id)->first();
        if($cash_book){
            $cash_book->delete();
        }
        Session::flash('successMsg', 'Bill Record Deleted Successfully');
        return response()->json(['success' => true, 'message' => "record deleted successfully", 'data' => null], 200);
    }

    public function editBillRecord($id)
    {
        $this->admin_data['packages']=Package::all();

        $this->admin_data['members'] = Member::all();
        $this->admin_data['bill_record'] = BillsRecord::select('m.*', 'bills_record.*','bills_record.name as member_name')->where('bills_record.id', '=', $id)->leftJoin('members as m', 'm.membership_no', '=', 'bills_record.membership_no')->first();
        return view('admin.bill_record.edit', $this->admin_data);

    }

    public function updateBillRecord(Request $request)
    {
        $bill_record = BillsRecord::where('id', '=', $request->bill_id)
            ->first();
        $bill_record->bill_no = $request->bill_no;
        $bill_record->date = isset($request->date) ? $request->date : '0';
        $bill_record->membership_no = isset($request->membership_no) ? $request->membership_no : '';
        $bill_record->name = isset($request->member_name) ? $request->member_name : '';
        $bill_record->package = isset($request->package) ? $request->package : '0';
        $bill_record->amount = isset($request->amount) ? $request->amount : '0';
        $bill_record->discount = isset($request->discount) ? $request->discount : '0';
        $bill_record->paid_amount = isset($request->paid_amount) ? $request->paid_amount : '0';
        $bill_record->due_amount = isset($request->due_amount) ? $request->due_amount : '0';
        $bill_record->remarks = isset($request->remarks) ? $request->remarks : '';
        $bill_record->save();

        Session::flash('successMsg', 'Bill record updated successfully');
        return response()->json(['success' => true, 'message' => 'Bill record updated successfully', 'data' => null], 200);
    }

    public function cashEntry()
    {
        $this->admin_data['cash_entry_list'] = CashBook::where('category','=',$this->admin_data['login_user']->category)->get();
       $this->admin_data['sum']=DB::table('cash_book')->sum(DB::raw('debit_amount-credit_amount'));
       
        return view('admin.user.cash_entry', $this->admin_data);

    }

    public function cashEntryPost(Request $request)
    {
        $book = new CashBook();
        $book->date = $request->date;
        $book->particular = isset($request->particular) ? $request->particular : '';
        $book->debit_amount = isset($request->debit_amount) ? $request->debit_amount : 0;
        $book->credit_amount = isset($request->credit_amount) ? $request->credit_amount : 0;
        $book->category=$this->admin_data['login_user']->category;
        $book->save();
        if($request->particular =='withraw'){
    $book = new BankAccount();
        $book->date = $request->date;
        $book->particular = isset($request->particular) ? $request->particular : '';
        $book->debit_amount = 0;
        $book->credit_amount = isset($request->debit_amount) ? $request->debit_amount : 0;
        $book->category=$this->admin_data['login_user']->category;
        $book->save();
       
        }
        if($request->particular=='deposit'){
             $book = new BankAccount();
        $book->date = $request->date;
        $book->particular = isset($request->particular) ? $request->particular : '';
        $book->debit_amount = isset($request->credit_amount) ? $request->credit_amount : 0;
        $book->credit_amount = 0;
        $book->category=$this->admin_data['login_user']->category;
        $book->save();
       
        }
        Session::flash('successMsg', 'Content saved successfully');
        return response()->json(['success' => true]);
    }
    public function profile($id){
        $this->admin_data['user']=Member::find($id);
        $this->admin_data['bill_detail'] = BillsRecord::where('membership_no', '=', $this->admin_data['user']->membership_no)->get();

        return view('admin.user.profile',$this->admin_data);
    }

    public function cashEntryList()
    {
        $this->admin_data['cash_entry_list'] = CashBook::where('category','=',$this->admin_data['login_user']->category)->get();
        return view('admin.user.cash_entry_list', $this->admin_data);
    }

    public function editCashEntryList($id)
    {
        $this->admin_data['cash_list'] = CashBook::find($id)->first();
        return view('admin.user.cash_list_edit', $this->admin_data);

    }

    public function updateCashEntryList(Request $request)
    {
        $cash_list = CashBook::where('id', '=', $request->cash_id)->first();
        $cash_list->date = $request->date;
        $cash_list->particular = $request->particular;
        $cash_list->debit_amount = $request->debit_amount;
        $cash_list->credit_amount = $request->credit_amount;
        $cash_list->save();
        Session::flash('successMsg', 'Cash List updated successfully');
        return response()->json(['success' => true], 200);
    }

    public function deleteCashEntryList(Request $request)
    {
        $cash_list = CashBook::where('id', '=', $request->record_id);
        $cash_list->delete();
        Session::flash('successMsg', 'Record Deleted Successfully');
    }
    public function monthlylist(Request $request)
    {
         // dd($request->year);
       $data=DB::table('cash_book')
                                              // ->where(DB::raw("(DATE_FORMAT(date,'%Y'))"),'<=',$request->year)
                                            ->whereYear('date','<=',$request->year)
                                              // ->whereMonth('date','<',$request->month)
                                         // ->whereDate('')
                                           // ->sum(DB::raw('debit_amount-credit_amount'));
                                            ->get();
                                            $this->admin_data['before_remaining']=0;
                                         foreach ($data as $key => $d) {
                                            $year=date('Y', strtotime($d->date));
                                            $month=date('m', strtotime($d->date));
                                            if($year< $request->year){
                                                $this->admin_data['before_remaining']+=$d->debit_amount-$d->credit_amount;
                                            }
                                            elseif($year == $request->year && $month<$request->month) {
                                               $this->admin_data['before_remaining']+=$d->debit_amount-$d->credit_amount;
                                            
                                            }   
                                        }
      
       $this->admin_data['cash_entry_list'] = CashBook::whereYear('date', '=', $request->year)
            ->where(function ($query) use ($request) {
                if (isset($request->month)) {
                    $query->whereMonth('date', '=', $request->month);
                }
            })
            ->where('category','=',$this->admin_data['login_user']->category)
            ->orderBy('date','asc')
            ->get();
            $this->admin_data['cash_entry_sum'] = CashBook::whereYear('date', '=', $request->year)
            ->where(function ($query) use ($request) {
                if (isset($request->month)) {
                    $query->whereMonth('date', '=', $request->month);
                }
            })
            ->where('category','=',$this->admin_data['login_user']->category)
            ->sum(DB::raw('debit_amount-credit_amount'));
            $this->admin_data['total']=$this->admin_data['before_remaining'] + $this->admin_data['cash_entry_sum'];

        $html=view('admin.user.ajax_cash_book_list',$this->admin_data)->render();
        return response()->json(['success' => true, 'message' => 'member name', 'data' => ['html' => $html]]);
    }

    public function cashEntryQuery(Request $request)
    {
        $this->admin_data['attribute'] = $request->all();
        $this->admin_data['cash_book'] = CashBook::whereYear('date', '=', $request->year)
            ->where(function ($query) use ($request) {
                if (isset($request->month)) {
                    $query->whereMonth('date', '=', $request->month);
                }
            })
            ->where('category','=',$this->admin_data['login_user']->category)
            ->get();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML(view('admin.user.pdf', $this->admin_data)->render());
        return $pdf->setPaper('A4', 'portrait')->stream();

    }

    public function query()
    {
        return view('admin.user.query', $this->admin_data);
    }

    public function userList()
    {
        $this->admin_data['members'] = Member::where('category','=',$this->admin_data['login_user']->category)
            ->orderBy('id', 'desc')->get();
        return view('admin.user.list', $this->admin_data);
    }

    public function searchQuery(Request $request)
    {
        $time = strtotime(date('Y-m-d'));
        $user_valid_date = date("Y-m-d", strtotime("+7 days", $time));
        $today_date=date("Y-m-d H:i:s");

        if ($request->type == 'due_bill') {

            $this->admin_data['bill_records'] = BillsRecord::select('members.*', 'bills_record.*')
                ->where('bills_record.due_amount', '!=', 0)
                ->leftJoin('members', 'members.membership_no', '=', 'bills_record.membership_no')
                ->orderBy('bills_record.id', 'desc')
                ->where('bills_record.category','=',$this->admin_data['login_user']->category)
                ->get();
            return view('admin.bill_record.list', $this->admin_data);

        } elseif ($request->type == 'paid_bill') {

            $this->admin_data['bill_records'] = BillsRecord::select('members.*', 'bills_record.*')
                ->where('bills_record.due_amount', '=', 0)
                ->leftJoin('members', 'members.membership_no', '=', 'bills_record.membership_no')
                ->orderBy('bills_record.id', 'desc')
                ->where('bills_record.category','=',$this->admin_data['login_user']->category)
                ->get();

            return view('admin.bill_record.list', $this->admin_data);

        } elseif ($request->type == 'days_expired') {
            $this->admin_data['bill_records'] = BillsRecord::select('members.*', 'bills_record.*')
                ->whereBetween('bills_record.valid_date',[$today_date,$user_valid_date])
                ->leftJoin('members', 'members.membership_no', '=', 'bills_record.membership_no')
                ->orderBy('bills_record.id', 'desc')
                ->where('bills_record.category','=',$this->admin_data['login_user']->category)
                ->get();
            return view('admin.bill_record.list', $this->admin_data);


        }elseif ($request->type == 'expired_bill') {
            $this->admin_data['bill_records'] = BillsRecord::select('members.*', 'bills_record.*')
                // ->whereYear('bills_record.valid_date', '<=', date('Y'))
                // ->whereMonth('bills_record.valid_date', '<', date('m'))
                ->where('valid_date', '<=',date('Y-m-d'))
                ->leftJoin('members', 'members.membership_no', '=', 'bills_record.membership_no')
                ->orderBy('bills_record.id', 'desc')
                ->where('bills_record.category','=',$this->admin_data['login_user']->category)
                ->where('bills_record.status','!=','Completed')
                ->orderBy('bills_record.created_at','desc')
                ->get();
            return view('admin.bill_record.list', $this->admin_data);


        }
        $this->admin_data['members'] = Member::where(function ($query) use ($request) {
            if ($request->type == 'Active') {
                $query->where('user_status', '=', 'Active');
            }
            if ($request->type == 'Deactive') {
                $query->where('user_status', '=', 'Deactive');

            }

        })->where('category','=',$this->admin_data['login_user']->category)

            ->get();
        return view('admin.user.list', $this->admin_data);

    }

    public function billDetail(Request $request, $membership_no)
    {
        $this->admin_data['bill_detail'] = BillsRecord::where('membership_no', '=', $membership_no)->get();
        return view('admin.user.bill_detail', $this->admin_data);

    }

    public function editUser($id)
    {
        $this->admin_data['user'] = Member::where('membership_no', '=', $id)->first();
        return view('admin.user.edit', $this->admin_data);
    }

    public function queryNotifications(Request $request)
    {
        $notifications = Notifications::where('id', '=', $request->id)->first();
        $notifications->status = 1;
        $notifications->save();
        if($notifications->type=='bill_expired'){
            return redirect()->route('admin.billRecord.edit', ['id' => $notifications->reference_id, 'token' => Session::get('token')]);


        }
        return redirect()->route('admin.user.edit', ['id' => $notifications->reference_id, 'token' => Session::get('token')]);
    }

    public function memberSelected(Request $request)
    {
        $member = Member::where('membership_no', '=', $request->membership_no)->first();
        return response()->json(['success' => true, 'message' => 'member name', 'data' => ['name' => $member->name]]);
    }

    public function updateUser(Request $request)
    {

        $user = Member::where('membership_no', '=', $request->membership_no)->first();


//
//            $image = $request->file('image');
//        $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
//        $destinationPath = public_path('/images');
//        $image->move($destinationPath, $input['imagename']);
//        $this->postImage->add($input);

        $user->name = $request->name;
        $user->address = isset($request->address) ? $request->address : '';
        $user->user_valid_date = $request->user_valid_date;
        $user->gender = $request->gender;
        $user->admission_date = $request->admission_date;
        $user->package_rate = isset($request->package_rate) ? $request->package_rate : ' ';
        $user->email = isset($request->email) ? $request->email : '';
        $user->contact = isset($request->contact) ? $request->contact : '';
        $user->user_status = $request->user_status;
            $user->blood_group = isset($request->blood_group)?$request->blood_group:'';
    
        $user->save();
        Session::flash('successMsg', 'User updated successfully');
        return response()->json(['success' => true, 'message' => 'User updated successfully', 'data' => null], 200);

    }

    public function viewNotifications()
    {
        $this->admin_data['all_not'] = Notifications::orderBy('id', 'desc')->where('category','=',$this->admin_data['login_user']->category)
            ->get();
        return view('admin.user.view_notifications', $this->admin_data);

    }

    public function bankAccount()
    {
        return view('admin.user.bank_account', $this->admin_data);

    }

    public function bankEntryStore(Request $request)
    {

        $book = new BankAccount();
        $book->date = $request->date;
        $book->particular = isset($request->particular) ? $request->particular : '';
        $book->debit_amount = isset($request->debit_amount) ? $request->debit_amount : 0;
        $book->credit_amount = isset($request->credit_amount) ? $request->credit_amount : 0;
        $book->category=$this->admin_data['login_user']->category;
        $book->save();
        Session::flash('successMsg', 'Content saved successfully');
        return response()->json(['success' => true]);
    }

    public function bankAccountList()
    {
        $this->admin_data['bank_account_list'] = BankAccount::where('category','=',$this->admin_data['login_user']->category)->get();
        return view('admin.user.bank_account_list', $this->admin_data);
    }

    public function editBankAccountList($id)
    {
        $this->admin_data['bank_list'] = BankAccount::where('id', '=', $id)->first();
        return view('admin.user.bank_account_edit', $this->admin_data);
    }

    public function updateBankAccountList(Request $request)
    {
        $bank_account_list = BankAccount::where('id', '=', $request->bank_id)->first();
        $bank_account_list->date = $request->date;
        $bank_account_list->particular = $request->particular;
        $bank_account_list->debit_amount = $request->debit_amount;
        $bank_account_list->credit_amount = $request->credit_amount;
        $bank_account_list->save();
        Session::flash('successMsg', 'Bank Account updated successfully');
        return response()->json(['success' => true], 200);
    }

    public function deleteBankAccountList(Request $request)
    {
        $bank_list = BankAccount::where('id', '=', $request->record_id);
        $bank_list->delete();
        Session::flash('successMsg', 'Record Deleted Successfully');

    }

    public function bankAccountQuery()
    {
        return view('admin.user.bank_account_query', $this->admin_data);

    }

    public function bankEntryQuery(Request $request)
    {
        $this->admin_data['attribute'] = $request->all();
        $this->admin_data['cash_book'] = BankAccount::whereYear('date', '=', $request->year)
            ->where(function ($query) use ($request) {

                if (isset($request->month)) {
                    $query->whereMonth('date', '=', $request->month);
                }
            })
            ->where('category','=',$this->admin_data['login_user']->category)
            ->get();
        $debit_amount = DB::table('cash_book')->whereYear('date', '=', $request->year)
            ->where(function ($query) use ($request) {

                if (isset($request->month)) {
                    $query->whereMonth('date', '=', $request->month);
                }
            })->where('category','=',$this->admin_data['login_user']->category)
            ->sum('debit_amount');
        $credit_amount = DB::table('cash_book')->whereYear('date', '=', $request->year)
            ->where(function ($query) use ($request) {

                if (isset($request->month)) {
                    $query->whereMonth('date', '=', $request->month);
                }
            })->where('category','=',$this->admin_data['login_user']->category)
            ->sum('credit_amount');
        $this->admin_data['total'] = $debit_amount - $credit_amount;
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML(view('admin.user.bank_pdf', $this->admin_data)->render());
        return $pdf->setPaper('A4', 'portrait')->stream();

    }

    public function import(Request $request)
    {
//        $member = Member::where('id', '>', 616)->get();
//        foreach ($member as $m) {
//            $m->user_status = 'Active';
//            $m->save();
//        }
//
        if (Input::hasFile('import_file')) {
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();
            foreach($data as $d){

//                $bil_record = new BillsRecord();
//                $bil_record->date = $d->date!=null ? nepali_to_english('2'.$d->date)['total_date'] : '';
//                $bil_record->membership_no =$d->membership_no!=null ? $d->membership_no : '';
//                $bil_record->bill_no = $d->bill_no!=null ? 'B'.$d->bill_no : '';
//                $bil_record->package = $d->package!=null ? $d->package: '';
//                $bil_record->name = $d->particular!=null ? $d->particular: '';
//
//                $bil_record->amount = $d->amount!=null ? $d->amount : '0';
//                $bil_record->discount = $d->discount!=null ? $d->discount : '0';
//                $bil_record->paid_amount = $d->paid_amount!=null ? $d->paid_amount : '0';
//                $bil_record->due_amount = $d->due_amount!=null ? $d->due_amount : '0';
//                $bil_record->remarks = $d->remarks!=null?$d->remarks:'';
//               $bil_record->valid_date = $bil_record->date;
//                $bil_record->save();
//if($d->paid_amount!=null){
//        $bill_record = new CashBook();
//        $bill_record->date = $bil_record->date;
//        $bill_record->particular = $d->particular!=null ? $d->particular : '';
//        $bill_record->debit_amount = $d->paid_amount!=null ? $d->paid_amount : '';
//        $bill_record->credit_amount = '0.00';
//        $bill_record->save();
//   }
 $user=Member::where('membership_no','=',$d->membership_no)->first();
if(!$user && $d->membership_no!=null){

  $input = [
                'membership_no', 'name', 'address', 'user_valid_date', 'gender', 'admission_date', 'package_rate', 'email', 'contact', 'photo', 'user_status', 'is_member'
            ];
            foreach ($input as $i) {

                $data3[$i] = isset($d[$i]) && $d[$i]!=''?$d[$i]:'';
            }

            $data3['is_member'] = 1;
          $data3['membership_no']=$d->membership_no;
         $data3['admission_date']=nepali_to_english($d['date'])['total_date'];
    $time = strtotime($data3['admission_date']);


    $data3['user_valid_date']=  date("Y-m-d", strtotime("+24 month", $time));
            $data3['user_status'] = 'Active';
            $data3['category']='gym';
            Member::create($data3);
}
            }
        }
        return back();

    }
}

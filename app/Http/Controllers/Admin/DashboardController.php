<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\BillsRecord;
use App\Models\Admin\BranchPara;
use App\Models\Admin\EmployeeParameter;
use App\Models\Admin\Member;
use App\Models\Admin\Notifications;
use Auth;
use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Session;
use Illuminate\Support\Facades\Input;
use DB;


class DashboardController extends Controller
{
    protected $admin_data;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->admin_data['login_user'] = Auth::user();
            $this->admin_data['notifications'] = Notifications::orderBy('id', 'desc')->get();
            $this->admin_data['n_count'] = DB::table('notifications')->where('status', '=', 0)->count();
            return $next($request);
        });
    }

    public function index()
    {
        if (Input::has('token')) {
            Session::put('token', Input::get('token'));
        }
        $time = strtotime(date('Y-m-d'));
        $user_valid_date = date("Y-m-d", strtotime("+7 days", $time));
         $today_date=date("Y-m-d H:i:s");
//             $user_data=EmployeeParameter::where('user_id','=',$this->admin_data['login_user']->id)->first();
//             Session::put('branch_code',$user_data);
        $this->admin_data['token'] = Session::get('token');
        $this->admin_data['branch_para'] = BranchPara::orderBy('branch_name', 'asc')->get();
        $this->admin_data['total_members']=Member::all()->where('category','=',$this->admin_data['login_user']->category)->count();
        $this->admin_data['active_members']=Member::where('user_status','=', 'Active')->where('category','=',$this->admin_data['login_user']->category)->count();
        $this->admin_data['deactive_members']=Member::where('user_status','=', 'Deactive')->where('category','=',$this->admin_data['login_user']->category)->count();
        $this->admin_data['due_members']=BillsRecord::where('due_amount','!=', '0.00')->where('category','=',$this->admin_data['login_user']->category)->count();
        $this->admin_data['paid_members']=BillsRecord::where('due_amount','=', '0.00')->where('category','=',$this->admin_data['login_user']->category)->count();
        $this->admin_data['validity_finished']=BillsRecord::where('valid_date', '<=',date('Y-m-d'))
                                                         ->where('category','=',$this->admin_data['login_user']->category)
                                                         ->where('bills_record.status','!=','Completed')
                                                        ->count();
        $this->admin_data['days_alert']=BillsRecord::whereBetween('valid_date',[$today_date,$user_valid_date])
            ->where('category','=',$this->admin_data['login_user']->category)
            ->count();
        return view('admin.dashboard', $this->admin_data);
    }

    public function selectBranch(Request $request)
    {
        User::where('id','=',Auth::user()->id)->update(['category'=>$request->branch_code]);
        Session::put('branch_code', $request->branch_code);
        Session::flash('successMsg', 'Type switched successfully changed');
        return response()->json(['success' => true, 'message' => 'Branch successfully changed', 'data' => null], 200);
    }
}
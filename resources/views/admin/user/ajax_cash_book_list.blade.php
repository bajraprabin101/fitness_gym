<?php $token=Session::get('token'); ?>
<table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Particular</th>
                                <th>Debit Amount</th>
                                <th>Date</th>
                                <th>Particulars</th>
                                <th>Credit Amount</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1;
                             ?>
                            <tr>
                                    <td></td>
                                    <td>Cash b/d </td>
                                    <td>{{$before_remaining}} </td>
                                    <td></td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <tr/>
<?php $debit_amount=$before_remaining;
$credit_amount=0; ?>
                            @foreach($cash_entry_list as $c)
                                <tr>
                                    @if($c->debit_amount!=0)
                                    <td>{{$c->date}}</td>
                                    <td>{{$c->particular}} </td>
                                    <td>{{$c->debit_amount}} </td>
                                    <?php $debit_amount+=$c->debit_amount; ?>
                                    @else
                                    <td></td>
                                    <td> </td>
                                    <td> </td>
                                    @endif
                                    @if($c->credit_amount!=0)
                                    <td>{{$c->date}}</td>
                                    <td>{{$c->particular}} </td>
                                    <td>{{$c->credit_amount}} </td>
                                    <?php $credit_amount+=$c->credit_amount; ?>
                                    @else
                                    <td></td>
                                    <td> </td>
                                    <td> </td>
                                    @endif
                                
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button"
                                                    data-toggle="dropdown">Action
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{route('admin.user.cash_entry.list.edit',['id'=>$c->id, 'token'=>$token])}}"
                                                       data-id="{{$c->id}}" class="btn-edit">Edit</a>
                                                </li>
                                                <li><a href="#" data-id="{{$c->id}}"
                                                       class="btn-delete">Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                    <td></td>
                                    <td></td>
                                    <td>{{$debit_amount}}</td>
                                    <td></td>
                                    <td> </td>
                                    <td> {{$credit_amount}}</td>
                                    <td> </td>
                                    <tr/>
                            </tfoot>
                        </table>
                        <h2>
                Cash Entry(Total Difference-<span class="label label-success">Rs{{$total}}</span>)
                <small></small>
            </h2>
                        
                        <script>
//        delete
        $('#example1').on('click', '.btn-delete', function (e) {
            var delete_id = $(this).attr('data-id');
            $('#myModal3').modal('show');
            $('[name="record_id"]').val(delete_id);

        });
        $('#delete_cash_record').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{route('admin.user.cash_entry.list.delete')}}",
                method: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + "{{$token}}");
                },
                data: $(this).serialize(),
                success: function (response) {
                    location.reload();
                    // $('.api_error_message').html('<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4><i class="icon fa fa-ban"></i>Success!</h4>'+response.message+' </div>');

                }
            })

        });
    </script>
@extends('admin.admin_layout.layout')
@section('content')
    <style>
        body{
            background: -webkit-linear-gradient(left, #3931af, #00c6ff);
        }
        .emp-profile{
            padding: 3%;
            margin-top: 3%;
            margin-bottom: 3%;
            border-radius: 0.5rem;
            background: #fff;
        }
        .profile-img{
            text-align: center;
        }
        .profile-img img{
            width: 70%;
            height: 100%;
        }
        .profile-img .file {
            position: relative;
            overflow: hidden;
            margin-top: -20%;
            width: 70%;
            border: none;
            border-radius: 0;
            font-size: 15px;
            background: #212529b8;
        }
        .profile-img .file input {
            position: absolute;
            opacity: 0;
            right: 0;
            top: 0;
        }
        .profile-head h5{
            color: #333;
        }
        .profile-head h6{
            color: #0062cc;
        }
        .profile-edit-btn{
            border: none;
            border-radius: 1.5rem;
            width: 70%;
            padding: 2%;
            font-weight: 600;
            color: #6c757d;
            cursor: pointer;
        }
        .proile-rating{
            font-size: 12px;
            color: #818182;
            margin-top: 5%;
        }
        .proile-rating span{
            color: #495057;
            font-size: 15px;
            font-weight: 600;
        }
        .profile-head .nav-tabs{
            margin-bottom:5%;
        }
        .profile-head .nav-tabs .nav-link{
            font-weight:600;
            border: none;
        }
        .profile-head .nav-tabs .nav-link.active{
            border: none;
            border-bottom:2px solid #0062cc;
        }
        .profile-work{
            padding: 14%;
            margin-top: -15%;
        }
        .profile-work p{
            font-size: 12px;
            color: #818182;
            font-weight: 600;
            margin-top: 10%;
        }
        .profile-work a{
            text-decoration: none;
            color: #495057;
            font-weight: 600;
            font-size: 14px;
        }
        .profile-work ul{
            list-style: none;
        }
        .profile-tab label{
            font-weight: 600;
        }
        .profile-tab p{
            font-weight: 600;
            color: #0062cc;
        }
    </style>
    <?php $token = Session::get('token'); ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User Record
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">General Elements</li>
            </ol>
        </section>
        <section class="content">
            <div class="container emp-profile">
                <form method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="profile-img">
                                <img src="{{url('gym.png')}}" alt=""/>
                                <div class="file btn btn-lg btn-primary">
                                    {{--Change Photo--}}
                                    <input type="file" name="file"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="profile-head">
                                <h5>
                                    {{$user->name}}
                                </h5>
                                <h6>
                                    <span class="label label-success">  {{$user->user_status}}</span>
                                </h6>
                                <p class="proile-rating">Membership No : <span>{{$user->membership_no}}</span></p>
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li>
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Bill List</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            {{--<div class="profile-work">--}}
                                {{--<p>WORK LINK</p>--}}
                                {{--<a href="">Website Link</a><br/>--}}
                                {{--<a href="">Bootsnipp Profile</a><br/>--}}
                                {{--<a href="">Bootply Profile</a>--}}
                                {{--<p>SKILLS</p>--}}
                                {{--<a href="">Web Designer</a><br/>--}}
                                {{--<a href="">Web Developer</a><br/>--}}
                                {{--<a href="">WordPress</a><br/>--}}
                                {{--<a href="">WooCommerce</a><br/>--}}
                                {{--<a href="">PHP, .Net</a><br/>--}}
                            {{--</div>--}}
                        </div>
                        <div class="col-md-8">
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <div class="row">
                                    <div class="col-md-6">
                                    <label>Membership No</label>
                                    </div>
                                    <div class="col-md-6">
                                    <p>{{$user->membership_no}}</p>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-6">
                                    <label>Address</label>
                                    </div>
                                    <div class="col-md-6">
                                    <p>{{$user->address}}</p>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-6">
                                    <label>Admission Date</label>
                                    </div>
                                    <div class="col-md-6">
                                    <p>{{$user->admission_date}}</p>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-6">
                                    <label>Email</label>
                                    </div>
                                    <div class="col-md-6">
                                    <p>{{$user->email}}</p>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-6">
                                    <label>Contact</label>
                                    </div>
                                    <div class="col-md-6">
                                    <p>{{$user->contact}}</p>
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>User Status</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>{{$user->user_status}}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>User Valid Date</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>{{$user->user_valid_date}}
                                              <input type="button" class="btn btn-primary status_change" value="{{$user->user_status=='Active'?'Deactive':'Active'}}"></p>
                                        </div>
                                    </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                            <label>Blood Group</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>{{$user->blood_group}}</p>
                                        </div>
                                    </div> <div class="row">
                                        <div class="col-md-6">
                                            <label>Package Expiry Date</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>{{getPackageDate($user->membership_no)}}</p>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div id="profile" class="tab-pane fade">
                                    <div class="row">
                                    <div class="col-md-10">
                                    <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                    <th>SNo</th>
                                    <th>Membership No</th>
                                    <th>Bill No</th>
                                    <th>Amount</th>
                                    <th>Discount</th>
                                    <th>Paid amount</th>
                                    <th>Due Amount</th>
                                    <th>Remarks</th>
                                    <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($bill_detail as $b)
                                    <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$b->membership_no}} </td>
                                    <td>{{$b->bill_no}} </td>
                                    <td>{{$b->amount}}</td>
                                    <td>{{$b->discount}}</td>
                                    <td>{{$b->paid_amount}}</td>
                                    <td>{{$b->due_amount}}</td>
                                    <td>{{$b->remarks}}</td>
                                    <td>
                                    <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button"
                                    data-toggle="dropdown">Action
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                    <li><a href="{{route('admin.billRecord.edit',['id'=>$b->id, 'token'=>$token])}}" data-id="{{$b->id}}" class="btn-edit">Edit</a>
                                    </li>
                                    <li><a href="#" data-id="{{$b->id}}"
                                    class="btn-delete">Delete</a></li>

                                    </ul>
                                    </div>
                                    </td>
                                    </tr>
                                    @endforeach
                                    </tfoot>
                                    </table>
                                    </div>
                                    </div>
                                </div>
                             </div>

                        </div>
                    </div>
                </form>
            </div>

        </section>

    </div>
    <script>
             var membership_no="{!! $user->membership_no !!}"; 
   
    $('#update_user').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: "{{route('admin.user.updateDetail')}}",
            method: "POST",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + "{{$token}}");
            },
            data: $(this).serialize(),
            success: function (response) {
                if(response.success==true){}
                location.reload();
                // $('.api_error_message').html('<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4><i class="icon fa fa-ban"></i>Success!</h4>'+response.message+' </div>');

            }
        })

    });
    $('.status_change').click(function(){
             $.ajax({
            url: "{{route('admin.user.status_change')}}",
            method: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + "{{$token}}");
            },
            data: {
                user_status:$(this).val(),
                membership_no:membership_no
            },
            success: function (response) {
                if(response.success==true){}
                location.reload();
           
            }
        })
         });
    </script>

@endsection
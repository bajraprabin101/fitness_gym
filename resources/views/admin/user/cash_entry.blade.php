@extends('admin.admin_layout.layout')
@section('content')
    <?php $token = Session::get('token'); ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Cash Entry(Total Difference-<span class="label label-success">Rs{{$sum}}</span>)
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">General Elements</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">

                @include('flash.message')
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Debit</h3>
                        </div>

                        <form role="form" id="update_user">
                            {!! csrf_field() !!}
                            <div class="box-body">

                                <br/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">Date</span>
                                            <input type="text" class="form-control" placeholder="Date" id="datepicker"
                                                   name="date" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">Particular</span>
                                            <input type="text" class="form-control" placeholder="Particular"
                                                   name="particular" required>
                                        </div>
                                    </div>
                                    <br/>
                                    <br/>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">Debit</span>
                                            <input type="number" class="form-control" placeholder="Debit"
                                                   name="debit_amount" value="0">
                                        </div>
                                    </div>
                                    {{--<div class="col-md-6">--}}
                                        {{--<div class="input-group">--}}
                                            {{--<span class="input-group-addon">Credit</span>--}}
                                            {{--<input type="text" class="form-control" placeholder="Credit Amount"--}}
                                                   {{--name="credit_amount" value="0">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                                <br>
                                <br>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Credit</h3>
                        </div>

                        <form role="form" id="credit_save">
                            {!! csrf_field() !!}
                            <div class="box-body">

                                <br/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">Date</span>
                                            <input type="text" class="datepicker form-control" placeholder="Date" id="datepicker_credit"
                                                   name="date" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">Particular</span>
                                            <input type="text" class="form-control" placeholder="Particular"
                                                   name="particular" required>
                                        </div>
                                    </div>
                                    <br/>
                                    <br/>
                                    {{--<div class="col-md-6">--}}
                                        {{--<div class="input-group">--}}
                                            {{--<span class="input-group-addon">Debit</span>--}}
                                            {{--<input type="text" class="form-control" placeholder="Debit"--}}
                                                   {{--name="debit_amount" value="0">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">Credit</span>
                                            <input type="number" class="form-control" placeholder="Credit Amount"
                                                   name="credit_amount" value="0">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                @include('flash.message');
                <div class="col-md-12">
                   <!--  <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Cash Entry Lists</h3>
                        </div>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Particular</th>
                                <th>Debit Amount</th>
                                <th>Credit Amount</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @foreach($cash_entry_list as $c)
                                <tr>
                                    <td>{{$c->date}}</td>
                                    <td>{{$c->particular}} </td>
                                    <td>{{$c->debit_amount}} </td>
                                    <td>{{$c->credit_amount}} </td>

                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button"
                                                    data-toggle="dropdown">Action
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{route('admin.user.cash_entry.list.edit',['id'=>$c->id, 'token'=>$token])}}"
                                                       data-id="{{$c->id}}" class="btn-edit">Edit</a>
                                                </li>
                                                <li><a href="#" data-id="{{$c->id}}"
                                                       class="btn-delete">Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tfoot>
                        </table>
                    </div> -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add query</h3>
                        </div>
                        <form role="form" 
                            
                              method="POST" id="post_method">
                            {!! csrf_field() !!}
                            <div class="box-body">
                                <br/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">Year</span>
                                            <select type="text" class="form-control" name="year" required>
                                                <option value="">--Select Year--</option>
                                                @for($i=2015;$i<=2075;$i++)
                                                <option>{{$i}}</option>
                                                    @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">Month</span>
                                            <select type="text" class="form-control" name="month" >
                                            <option value="">--Select Month--</option>
                                                <?php
                                                $month=['1'=>'January','2'=>'Februry','3'=>'March','4'=>'April',
                                                    '5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September',
                                                    '10'=>'October','11'=>'November','12'=>'December'];
                                                ?>
                                        @for($i=1;$i<=12;$i++)
                                                <option value="{{$i}}">{{$month[$i]}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <button type="submit" class="btn btn-primary" style="margin-left:10px">Query</button>
                                </div>
                                <div class="cash_list_html"></div>
                            </div>
                        </form>

                    </div>
                </div>
            
                </div>

            </div>

        </section>
    </div>
    <div id="myModal3" class="modal custom fade">
        <div class="modal-dialog" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete Information</h5>
                </div>
                <form id="delete_cash_record">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        <input name="record_id" type="hidden"/>

                        <p>Are you sure to delete this information?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Delete</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $('#post_method').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{route('admin.user.cash_entry.monthlylist')}}",
                method: "GEt",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + "{{$token}}");
                },
                data: $(this).serialize(),
                success: function (response) {
                    if (response.success == true) {
                        console.log(response);
                        $('.cash_list_html').html(response.data.html);

                        // location.reload();
                    }
                    // $('.api_error_message').html('<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4><i class="icon fa fa-ban"></i>Success!</h4>'+response.message+' </div>');

                }
            })

        });
        $('#update_user').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{route('admin.user.cash_entry.store')}}",
                method: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + "{{$token}}");
                },
                data: $(this).serialize(),
                success: function (response) {
                    if (response.success == true) {

                        // location.reload();
                    }
                    // $('.api_error_message').html('<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4><i class="icon fa fa-ban"></i>Success!</h4>'+response.message+' </div>');

                }
            })

        });
        $('#credit_save').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{route('admin.user.cash_entry.store')}}",
                method: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + "{{$token}}");
                },
                data: $(this).serialize(),
                success: function (response) {
                    if (response.success == true) {

                        location.reload();
                    }
                    // $('.api_error_message').html('<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4><i class="icon fa fa-ban"></i>Success!</h4>'+response.message+' </div>');

                }
            })

        });
    </script>

@endsection
@extends('admin.admin_layout.layout')
@section('content')
    <?php $token = Session::get('token'); ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User Record
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">General Elements</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <!-- left column -->
                        <div class="box-body box-profile">
                            <h3 class="profile-username text-center">Membership No:{{$bill_record->membership_no}}</h3>
                            <img class="img-responsive" src="{{url('gymlogo.gif')}}" style="height:400px;" alt="User profile picture">


                            <h3 class="profile-username text-center">{{$bill_record->name}}</h3>

                            <p class="text-muted text-center"><span class="label label-success">{{$bill_record->user_status}}</span></p>

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Remaining Days</b> <a class="pull-right">
                                        @if($date_diff>7)
                                            <span class="label label-success">  {{$date_diff}} days</span>
                                    @elseif($date_diff<=7 && $date_diff>=1)
                                            <span class="label label-warning">  {{$date_diff}} days</span>

                                    @else
                                            <span class="label label-danger">  {{$date_diff}} days</span>

                                        @endif
                                    </a>
                                </li> <li class="list-group-item">
                                    <b>Address</b> <a class="pull-right">{{$bill_record->address}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Gender</b> <a class="pull-right">{{$bill_record->gender}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Admission Date</b> <a class="pull-right">{{$bill_record->admission_date}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Email</b> <a class="pull-right">{{$bill_record->email}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Contact</b> <a class="pull-right">{{$bill_record->contact}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>User Status</b> <a class="pull-right"><span class="label label-success">{{$bill_record->user_status}}</span></a>
                                </li>
                                <li class="list-group-item">
                                    <b>User Valid Date</b> <a class="pull-right"><span class="label label-success">{{$bill_record->user_valid_date}}</span></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Record Date</b> <a class="pull-right"><span class="label label-success">{{$bill_record->date}}</span></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Package</b> <a class="pull-right"><span class="label label-success">{{$bill_record->package}}</span></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Bill NO</b> <a class="pull-right"><span class="label label-success">{{$bill_record->bill_no}}</span></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Amount</b> <a class="pull-right"><span class="label label-success">{{$bill_record->amount}}</span></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Paid Amount</b> <a class="pull-right"><span class="label label-success">{{$bill_record->paid_amount}}</span></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Discount</b> <a class="pull-right"><span class="label label-success">{{$bill_record->discount}}</span></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Due Amount</b> <a class="pull-right"><span class="label label-success">{{$bill_record->due_amount}}</span></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Remarks</b> <a class="pull-right"><span class="label label-success">{{$bill_record->remarks}}</span></a>
                                </li>
                                    <li class="list-group-item">
                                        <b>Valid Date</b> <a class="pull-right"><span class="label label-success">@if(isset($bill_record->valid_date)){{date("F j, Y", strtotime($bill_record->valid_date))}}@endif</span></a>
                                    </li>

                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
    $('#update_user').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: "{{route('admin.user.updateDetail')}}",
            method: "POST",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + "{{$token}}");
            },
            data: $(this).serialize(),
            success: function (response) {
                if(response.success==true){}
                location.reload();
                // $('.api_error_message').html('<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4><i class="icon fa fa-ban"></i>Success!</h4>'+response.message+' </div>');

            }
        })

    });
    </script>

@endsection
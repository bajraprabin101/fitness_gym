@extends('admin.admin_layout.layout')
@section('content')
    <?php $token = Session::get('token'); ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Bill Records
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">General Elements</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="container">
                    <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{route('admin.user.import',['token'=>$token])}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <input type="file" name="import_file" />
                        <button class="btn btn-primary">Import File</button>
                    </form>
                </div>
            </div>
            <div class="row">

                <!-- left column -->
                <div class="col-md-12">
                    @include('flash.message')
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Bill Record</h3>
                        </div>
                        <div class="box-body">
                            <br/>
                            <form role="form" id="bill_record_submit">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="error_msg alert alert-success alert-dismissible fade in"
                                         style="display:none"><a href="#" class="close" data-dismiss="alert"
                                                                 aria-label="close">&times;</a> <strong>Danger!</strong>
                                        This alert box could indicate a dangerous or potentially negative action.
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group input-group-sm">
                                                 <span class="input-group-btn">
                                                     <button type="button" class="btn btn-flat pwd_click">
                                                        Date
                                                    </button>
                                                 </span>
                                            <input type="text" id="datepicker" class="form-control"
                                                   placeholder="Date" name="date" required>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="input-group input-group-sm">
                                                 <span class="input-group-btn">
                                                     <button type="button" class="btn btn-flat pwd_click">
                                                         Bill No.
                                                    </button>
                                                 </span>
                                            <input type="text" class="form-control"
                                                   placeholder="Bill number" name="bill_no" required
                                                    value="{{$bill_no}}">
                                        </div>
                                    </div>
                                    @if($login_user->category=='fitness')
                                        <div class="col-md-2">
                                            <div class="input-group input-group-sm">
                                                <select name="category_select" class="form-control dest_select_2" required>
                                                    <option value="">Select Category</option>
                                                  <option value="item">Item</option>
                                                  <option value="package">Package</option>
                                                </select>
                                            </div>
                                        </div>

                                    <div class="col-md-4 item_category">
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon">Item Category</span>
                                            <select name="item_id" class="form-control dest_select_2">
                                                <option value="">Select Item</option>
                                                @foreach($items as $m)
                                                    <option value="{{$m->id}}" data-price-id="{{$m->price}}"
                                                            required>{{$m->item_name}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                        @endif
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">Membership No</span>
                                            <select name="membership_no" class="form-control dest_select_2">
                                                <option value="">Select Membership</option>
                                                <option value="">No member</option>
                                                @foreach($members as $m)
                                                    <option value="{{$m->membership_no}}"
                                                            required>{{$m->membership_no}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="input-group input-group-sm">
                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-flat pwd_click">
                                    Member Name
                                    </button>
                                    </span>
                                            <input type="text" class="form-control"
                                                   placeholder="Member Name" name="member_name" required>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">D.Change Type</span>
                                            <select class="form-control valid_date_change_type">
                                                <option value="Current Selected Date">Current Selected Date</option>
                                                    <option value="User Old Date"
                                                            required>User Old Date</option>
                                               
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 package_category">
                                        <div class="input-group">
                                            <span class="input-group-addon">Package</span>
                                            <select name="package" class="form-control" required>
                                                <option value="">--Select Package--</option>
                                    @foreach($packages as $p)
                                                    <option value="{{$p->month}}" required>{{$p->month}}</option>
                                           @endforeach
                                                <option value="Due" required>Due</option>
                                                         <option value="Others" required>Others</option>
                                     <option value="Per Day" required>Per Day</option>
                                       
                                            </select>
                                        </div>
                                    </div>


                                </div>
                                <br/>
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">Total Amount</span>
                                            <input type="number" value="0" class="form-control" placeholder="Amount"
                                                   name="amount">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">Paid Amount</span>
                                            <input type="number" class="form-control" value="0" placeholder="Paid Amount"
                                                   name="paid_amount">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">Discount</span>
                                            <input type="number" value="0" class="form-control" placeholder="Discount amt"
                                                   name="discount">
                                        </div>
                                    </div>


                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">Due Amount</span>
                                            <input type="number" class="form-control" value="0" placeholder="Due Amount"
                                                   name="due_amount">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">Remarks</span>
                                            <input type="text" class="form-control" placeholder="Remarks"
                                                   name="remarks">
                                        </div>
                                    </div>
                                    <div class="col-md-4 valid_date">
                                        <div class="input-group">
                                            <span class="input-group-addon">Valid Date</span>
                                            <input type="text" class="form-control" placeholder="Valid Date"
                                                   name="user_valid_date">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Add Bill</button>
                                    <button type="button" class="btn btn-primary cancel_bill">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    </section>
    </div>

    <script>
        $(document).ready(function () {
            var base_url = "{{url('/')}}";

            $('#bill_record_submit').submit(function (e) {
                console.log($(this).serialize());
                e.preventDefault();
                $.ajax({
                    url: "{{route('admin.billRecord.store')}}",
                    method: "POST",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'Bearer ' + "{{$token}}");
                    },
                    data: $(this).serialize(),
                    success: function (response) {
//                        console.log(response);
                        location.reload();
                        // $(".error_msg").text('Successfully saved');
                        // $(".error_msg").fadeIn(300).delay(1500).fadeOut(400);
                        // $('.api_error_message').html('<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4><i class="icon fa fa-ban"></i>Success!</h4>'+response.message+' </div>');


                    }
                })
            });
            $('.cancel_bill').click(function (e) {
                console.log($(this).serialize());
                e.preventDefault();
                $.ajax({
                    url: "{{route('admin.billRecord.cancel')}}",
                    method: "GET",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'Bearer ' + "{{$token}}");
                    },
                    data:{
                        bill_no:$('[name=bill_no]').val()
                    },
                    success: function (response) {
//                        console.log(response);
                        location.reload();
                        // $('.api_error_message').html('<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4><i class="icon fa fa-ban"></i>Success!</h4>'+response.message+' </div>');


                    }
                })
            });

            $('[name=category_select]').on('select2:select', function (e) {
             console.log($(this).val());
             if($(this).val()=='item'){
                 $('.item_category').show();
                 $('.package_category').hide()
                 $('.valid_date').hide();
                 $('[name=package]').prop('required',false);

             }
             else{
                 $('.item_category').hide();
                 $('.package_category').show();
                 $('.valid_date').show();

             }
            });
            $('[name=item_id]').on('select2:select', function (e) {
               $("[name=amount]").val($(this).select2('data')[0].element.dataset.priceId);
            });



            $('[name=package]').keyup(function (e) {
                e.preventDefault();
                packageSelected($(this).find('option:selected').val());

            });
            $('[name=package]').change(function (e) {
                e.preventDefault();
                packageSelected($(this).find('option:selected').val());

            });


            function packageSelected(package) {
                $.ajax({
                    url: "{{route('admin.user.package.selected')}}",
                    method: "GET",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'Bearer ' + "{{$token}}");
                    },
                    data: {
                        membership_no:$('[name=membership_no]').find('option:selected').val(),
                        month: package,
                        admission_date: $("[name=date]").val(),
                        valid_date_change_type:$('.valid_date_change_type').find('option:selected').val()
                    },
                    success: function (response) {
                        if (response.success == false) {
                            $('[name=amount]').val("");
                            $('[name=user_valid_date]').val("");

                        }
                        console.log(response);
                        $('[name=amount]').val(response.data.price);
                        $('[name=user_valid_date]').val(response.data.user_valid_date);
                        // $('.api_error_message').html('<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4><i class="icon fa fa-ban"></i>Success!</h4>'+response.message+' </div>');


                    }
                });
            }


                $('[name=amount],[name=discount],[name=paid_amount]').keyup(function () {

                    var package_rate = $('[name=amount]').val();
                    var discount = $('[name=discount]').val();
                    var paid_amt = $('[name=paid_amount]').val();
                    var due_amt = package_rate - discount - paid_amt;
                    $('[name=due_amount]').val(due_amt);

                })



        $('.dest_select_2').on('select2:select', function (e) {
            console.log('test');
            var data = e.params.data;
            e.preventDefault();
            $.ajax({
                url: "{{route('admin.user.member.selected')}}",
                method: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + "{{$token}}");
                },
                data: {
                    membership_no: $(this).val()

                },
                success: function (response) {
                    if (response.success == true) {
                        $('[name=member_name]').val(response.data.name);

                    }
                    // $('.api_error_message').html('<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4><i class="icon fa fa-ban"></i>Success!</h4>'+response.message+' </div>');


                }
            })
        });

        })
        ;
    </script>
@endsection
